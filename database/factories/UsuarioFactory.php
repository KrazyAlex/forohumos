<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\usuario;
use Faker\Generator as Faker;

$factory->define(usuario::class, function (Faker $faker) {
    return [
        'nomUsuario' => $faker->sentence(1),
        'contraseña'=> $faker->sentence(1),
        'correo' => $faker->sentence(1),
        'nombre' => $faker->sentence(1),
        'apellido' => $faker->sentence(1),
        'fechaNac' => $faker->sentence(1),
      ];
});
