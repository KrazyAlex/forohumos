<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\usuario;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(usuario::class)->create(
            ['nomUsuario' => 'Dartigas',
            'contraseña' => '123456',
            'correo'=> 'dartigas19@gmail.com',
            'nombre'=> 'Daniel',
            'apellido'=> 'Artigas',
            'fechaNac'=> '15-02-1997']);
    }
}
