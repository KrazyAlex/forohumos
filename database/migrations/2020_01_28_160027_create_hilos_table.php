<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHilosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hilos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('userCreator')->unsigned();
            $table->string("tema");
            $table->bigInteger('etiquetaid')->unsigned();
            $table->string("texto");
            $table->integer("likes")->default(0);
            $table->timestamps();
        });

        Schema::table('hilos', function($table) {
            $table->foreign('userCreator')->references('id')->on('usuarios')->onDelete('cascade');
            $table->foreign('etiquetaid')->references('id')->on('etiquetas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hilos');
    }
}
