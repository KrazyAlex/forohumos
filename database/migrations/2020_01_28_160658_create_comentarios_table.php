<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idHilo')->unsigned();
            $table->bigInteger('idComent')->unsigned();
            $table->bigInteger('userCreator')->unsigned();
            $table->string("texto");
            $table->integer("likes");
            $table->integer("dislikes");
            $table->timestamps();
        });



        Schema::table('comentarios', function($table) {
            $table->foreign('idHilo')->references('id')->on('hilos')->onDelete('cascade')->nullable(true);
            $table->foreign('idComent')->references('id')->on('comentarios')->onDelete('cascade')->nullable(true);
            $table->foreign('userCreator')->references('id')->on('usuarios')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarios');
    }
}
