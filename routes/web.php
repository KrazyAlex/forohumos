<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    session(['key' => ""]);
    return view('welcome');
})->name('welcome');

Route::get('users', 'usuarioController@index')->name('listaUser');

Route::get('users/delete/{id}', 'usuarioController@destroyFromAdmin');

Route::get('register', function () {
    return view('register');    
});

Route::post('register', 'usuarioController@create');

Route::get('login', function () {
    return view('login');
});

Route::post('login', 'usuarioController@finduser');

Route::get('mainPage', 'hiloController@index')->name('mainpage');

Route::get('userPage', 'usuarioController@edit')->name('editpage');

Route::post('userPage', 'usuarioController@update');

Route::post('/userPage/updateIMG', 'usuarioController@updateIMG');

Route::get('mainPage/hilo/{id}', 'hiloController@show')->name('showHilo');

Route::get('mainPage/delete/{id}', 'hiloController@destroy');

Route::get('mainPage/create/hilo','hiloController@createPage');

Route::post('mainPage/create/hilo', 'hiloController@create');

Route::post('/createCom/{idHilo}/{id}', 'comentarioController@create');

Route::get('/search', 'hiloController@searchview');

Route::post('/search', 'hiloController@search');

Route::get('/userstop','usuarioController@top');

Route::get('/userstop/show/{username}','usuarioController@topshow');

Route::post('/usertop/show/{username}/add','usuarioController@addFriend');

Route::post('/usertop/show/{username}/delete','usuarioController@deleteFriend');

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/home', 'HomeController@index')->name('home');


Route::get('mainPage/hilo/{id}/like', 'likeController@addLike');

Route::get('mainPage/hilo/{id}/dislike', 'likeController@dislike');