<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\comentario;

class comentarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $idHilo, $id)
    {
        if($this->testLogin()){
            return redirect('mainPage/hilo/'.$id);
        }

        $newCom = new comentario;

        $newCom->idHilo = $idHilo;
        if($id == 0){
            $newCom->idComent = null;
        } else {
            $newCom->idComent = $id;
        }
        $newCom->userCreator = session('key');
        $newCom->texto = $request->input('comment');
        $newCom->likes = 0;
        $newCom->dislikes = 0;

        $newCom->save();

        return redirect('mainPage/hilo/'.$idHilo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function testLogin(){
        $userlog = session('key');


        if(strcmp($userlog, "") == 0){
            return true;
        } else {
            return false;
        }
    }
}
