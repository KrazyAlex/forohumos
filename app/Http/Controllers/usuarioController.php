<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\usuario;
use App\amigo;

use Illuminate\Support\Facades\DB;

class usuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($this->testLogin()){
            return redirect()->route('welcome');
        }


        $user = usuario::where('id', session("key"))->get()[0];
        $usuarios = usuario::all();
        return view('pruebas')->with('usuario', $user)->with('usuarios',$usuarios);
    }

    // echo '<script>console.log("'.$txtSearch.'")</script>';

    public function finduser(Request $request){
        
        $username = $request->input("username");
        $password = $request->input("password");

        $userfinded = usuario::where('nomUsuario', $username)->get();

      //  echo '<script>console.log(\''.$userfinded.'\')</script>';
 


        if(count($userfinded) == 0){
            //Si no encuentra en usuario
                return view('login')->with('error', 1);
        } else {
            //Si encuentra usuario

            $userfinded = $userfinded[0];

            if(strcmp($password, $userfinded->contraseña) !== 0){

                return view('login')->with('error', 2);
            } else {
               //return view('index')->with('usuario',$userfinded);

               session(['key' => $userfinded->id]);

               return redirect()->route('mainpage');
            }

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $usuario = new usuario;

        $usuario->nombre = $request->input("nombre");
        $usuario->apellido = $request->input("apellido");
        $usuario->fechaNac = $request->input("fechaNac");
        $usuario->nomUsuario = $request->input("username");
        $usuario->contraseña = $request->input("password");
        $usuario->correo = $request->input("correo");
        $usuario->img = 'iVBORw0KGgoAAAANSUhEUgAAALMAAACrCAIAAACi8zpSAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAALy9JREFUeF7tnV2obltZx5eI7u3H8XBOKnqhUREVfUCFetFF34UoKecsCixQSLzz3Gg3FpERhFhBRV4lZBBRSARBdSFCFwUGQRmUFFJZ9KHuLXu1dh4/9t79nvEfc7xjjueZ8x3zfef7cc7Zk+VxrXePd8wxnvEfz/d4xvMePHhwdXV17969i+p5/vOf/7KXvaz+hN/v3r375S9/ufnwscceaz55+umnv/jFL9Yf8orHH3+8afaVr3zl+vq6afboo4/y6vpDBnbnzp3nPe955UN6e+SRR17wghc0Hd6+fbtuRj8vetGLSrO7V3c/9z+3b3/uC5/+1Gc+/9nb/3v77mf/6/O3/vPq9mevvnTnS0//31eevv5S6fDmS2/cfPELbjx647FXPfLyVz36yle//JHHX/LyVz7+Dd/82sdf8dirXvOKmzdvqjGzYLI19U4yWV764he/uIyqTOQLX/hCQ6UXvvCFL3nJS5oPQwBc0Cmkh6z1wyd83jwsZNOMP30zKNU0u3Xrlm8GyHyzr371q01LPuHrdUv+5Lu+Q9ow7Ou7V/r51N/+85999OMf+uWPvPvHf/47Lt7y6ovv/9qLH/mWizd++8Wb9fOdF29+3cUT/Lz+4q1vuHH5hhtPVj+XfPi6i8vvvngrP3xdX/m6ix9+7cUP0hUf0i2d8wpeBGXspdfXomQnTfaZrKcJZA9p0hCZQfpmIQCeyci4P5oj0/ubv/y7j/zmHwgKLKFwwCqyxrb2N5/IP4aD8Sf80wgZoGRoU0BTfV2goXNeAVz45amf+DlezQDGm+regzRIoOzX8iEybq/OM4b9cQ+iswn+6mN//f6nPsjygAZBIbGBmgcc+ncDCihhAIDyfe/6AEO6vlN2572Qjz5ExsrISJvSaA0nhz0gI74x7dqjo8GjzdgM7ITBMCRQwvAYZILIPf7byMRnBjIaXjelZzTNpmSq55yhnrGUwYIGBsZ2ZFNKaWCD1sqB/R7IhUMzjNJ/rayY8EKVASL8MOCP/elfmBo06GrM/fjI4KVTeoYHwAXkNgVq/PBh+HS2XLcZI4GIn/3vzyHIf/Q1bwcTbMpBD9hz1W0tk7Jpqmj9M3CgRjPd7XWXQAR1hMEzBSYinndWFPYAuDhn8MrW+Jd/+swHfuZDUvdYvC4FwnEOvlisDPoRt2e1+D2pqHQ7qKg3nkQiwI2kXfJDYymzfDg0riCS3zWDISmzdPuE+BzTYVJigUXEsBBnxap7kcEEQlvRy5c9zSd1KMaLQfien/pFKf99aoSWx1aC9sV8YD348y3f9S4EP7v2j3/vzxFJrM2//9t/3Ll1xQ+/sJXLj/7U5zSjMV+R1UMnLHDBygZV3QovI5GyzNRk8TLZMut9nAXrCvGzQ4b4RMFE0iR6WbrQIBsBiLz9h97zW7/0u+iA//gP/8RKS25WpMe0MesGUeX9NF4JMDvo7hWd0FVxlvAKXlRsoi6WZjzGsMvUAH3Gx90rJr6nG+lZiQxbIUlftun73/3rbEpkcxcmbpqrSlTmv+9803v/8MN/wkaX0Vg7oFj+LDrHjhDvbHAM0jBU+9zE0uicVzBmXsdLebW8YQxj4z0LNWK5RhJ7kyHDlJl4xZVFEHv6HYzPSmQkGtx/AMeWt3ELJoZtJ+UftYCdN3YhWH+sWQ+xQjdryDO2ik7gKNOJIQERGFgXuG88CZj4YfryjKX/ZnCcIzIgRP1M6RlNsyk9o
        2nWmE94D9/0Te/s1DElMiC9HAaw96q3LCMKMpr3ekEuZDTPFDKmJ6v3Jufb3SuGJHcL49w2qeyNlX4KESBFPUim1rx0xlmwdbJikFsny/Szd1xCrn4kgMsjtra1Ge2lctcPM6m70u+Km0hss8kQ1WmHzZiFZjvQBkBAPrg3GuJAQVsS6a3SGPSLMNrMwk9BqFpxsgUidMsgGSoDHliIFOTQS2YfMkFIAUEM7sRi7lxrQzYE1BzL0zlZmoWT9TThk1PFTe4xYWbOxmKvbBcfN8z9DHHR+HAZDdxrI4yhnRccoVfHh/FCva+2J8tXPL/xrJ5h5GZZItggGTAzRRHBGdOjPyFZkEfMFBL1yDVe2mMS0uycI2p5OZkJO4PFNn0+dlxmExRMoEwQsoLNim8lZGykhlaiExl+yQ+FDKkLCR8MWEorU2AilQoyaXZBFppBorFamqnnJ9uDjMIzGoiHNDk6z0gaFso8DsFvvXjjtIJmskMhU6S1YSKxVtkXPZ6VRfvDM4MVeEbVaUk5ED4wepnUIEDnXGRwUwgFuarODBzPPmTYrOCrA1Od0CpumqOaHfO273mqZD/UvsJnLjKKGiR8MEFIMe0Fkef0EpaJqy2BI3HK+0dERoPBKZeLN9v8bpu3FXEMV3sl1sXYKBALAE2JiU5kdDLYfSYrw6EJovbQJNkXeZ8oPDsZCUr7BKJBuqJxr84zPAAuUHC80cEn3poIbRPfzNsmkJ5m/Pcnv+8p82lmb0TAMAoJ5KcCAZLQzaMYW/N0Ntt5siFNpibbjC1sxtSkXdJztWGmrDPjHO/4gfeWmD7itZ5yJ006V5Zmc97xFf0ZSEr2hKnlk7C4xJpHpkqlkD7BM7Ohy/DUeGqnbjXxV/JnbN7vHQZTFoFaSjFk4kwfIsw4xxJrefLTf/+vO092gT+DkXm7yLsF94moIVCVahXb8Ylbklz5a+/7bW2CmjlPIcNHOnp4eOdkw2Ze7ytehObVXub22IqmnN65hgiQYib+ojAhJOWlaeE2tl6PXOvPOzx4RA1vcTZNcxR0zC2TAcJU5fjz7oFOJWCGZzRKQM82OAkyzJhKgRhIoXD/hN/PwsioHZ/4+CdrRHaGAs4FGWiRzCHKgsm+CpgnykdJmex0Nuys950zz5BbU4sNQSBLkiyh2kFOoemkRUnnK88kZDBuGGOcV5ECpPzrb/zC79TAf4iMYn3wC8SBRFNqBwTE4h3Aca9zt6zDM/bRQMUtZmbFnMusCjhARvPSHg1UX9lZKTu5BlqmPEx2E5zT7ppUO26afiYy+sBbSJMFGqisVv8o6FU/nc3QidAtZiJkSFDAnnQoQ3p55Pb2T/NemslCa1rKvVg/vtnqk1U2UDMLP7aw2fxkpYxDIgiFvTqoHSklcSNiTChDTAguH3z9dE52qlngHZ+xFXssAhSoSuVsxKQFxmAkypLlv7uph52ertAi2C2iJrXfs6XVeXhkXxihIJqRLjoxJbEiFb7Rr/1ke6wkAWDluAl+ixQnjPN4sUGIR4tn9gs8T6x9kNGpyvhmU8jw1PcAKnGT2sffaSXJk4tO+savf8dEnNasFWg7Dq/YKPxO7kGGdOG1kGFWNbkIGdpRCgJD/7Fve9dghhjr8zTtJNZzChllspAOApqzKwpNixkrZ6X4Oc4BGYznHvGh8dGgjSgRLBL8ddTzITK6HIzwy2obWJ4sZJyIMFjwhSXImYIppn1yZBjDIMe18uyOTmvBAwdYiNHmZBYvJjp5Rhgq857HsJkXEyGD7fS5dXoRwmadk20YpDhHBkfLPCzCwEIUIp8WGbbMJLShBIX6EbD43q95W+Jyln5dZLCXJkwjJJafXmcQ9QjI8AAK1RFG4mfROVkvOgEHEZap9GOsQpZ
        DdN4LGTNuwR5/BlsQrXgCFjoYaJaIdmrd4XyQyZn4I62u03bXTm0erx6enz9jy2SZPiTVocswFAU4XL2G3OcCf0anwziKqJlDhiHCEhx+83lRmJtiP/uAN7QVe+xnv1P3mKyNolNg+Wb9FkH/ZOXnCF3MIIZFkWsgH1MY+u03CXeOqOVXctBDoWH/U3y3bMrVkRHZ/S1V95HuntWfGzIYjzyKoaOZRWFpisJRC/FO9W4PZKS8PZ0T3GgYQ/oFzi6KFQ0DspT/5tlzGz1EhhR5iAypA3DctHTJOuSmtTgGz0CpVFpeyy1u2ikBMmDrLfwQGV5MdPrctiq5kDrlWrdsW0ebpPuXNI51kDGvgTKg5JLzA7pEyBWPVlGSm95W1ECl23rSPys1UO2xerKQGoKHWxTjdtiiOcFngQYK+ZY+yGDYVGJigXoBTlGMGYHCSEs7P1r7emwKRJXkTf3ZjEQH4c/q0RQYKgRHG80lQGoH9E1L49DZraUj33jH6z09n2NHy4liiU8CCySfjIK6Q7+h+8ErlaLubcY7vtUwVlf81wKkqVQjh9D5QdVHodMPhOZPwhD5CKEqSW4qsm1m02k/L5psj7Og0EQshOFRDyJ7Gsfur1RI6LK072fVi+MmDIJE51CwYbvinQ0DIiEyDh03CT1dxsnu2FkP3EFwWoWpVFtHtXh0Rll/sguZERk0wCUZgYH542dx/MkyBubFUIMcwaT2sWTKu+5X/JchA15ClZLAUkomCdRkn50PMpRWOehfJmhZXViazh+rMFKuwzRxClk1nEpVjJ/96V+pDqqnY+/jin1TGs8Rwoe8AuKrupCvbUqCDwunFJZOXXgZMqA1x3bDsBlj+vCvfpS3doYSjkCsmgpQjTOiKrfVX9aiJjEoUf0W2EyxBiFIj/18hMmKQYL7MHtUNWeMX+bdMoJHaCUtQAb9ZtdKE8hJxQugl7xDoedxRQYrzuk7DDyPiQqYbfBSlcLZlA7uLqsVWoMIHSIXyqTqkSY7e2OZVE/KwUATi0yxEGHeuVzmnbGkTX5Go/WEGqji7NFbL/VWrVanrXgIpWwTbbllW5kBs7lRiTqqMCwu56icRTQVtNdGD+1Ut32zMIIzhYxmycqSD3ZKOyMGTPm54tio3x6m3/aeUWPyHHCwyJnbbbwSTiW+yhOisjEuaNavJHsKNisRS/f7D1ALUnptbwG4rnqS4+lLNg27YlPC4KDOm8afUW9I2SmxxL9pJQnhc3V0U6QLo7693vFJ1Tcpnqj6RVZNIWNd77h3qkKgksuEBIGpzlZhWMwkAtwkvRvkgb8h8B2UMFhkEYTbIJxs07IILBZCwbZ8UHQj+i2T1BuPeyFjE2p3DEMF2Gs5ehxkTOh9tjDKRe2rH7oGPpJdgzzl4KFWK9B4ui2CfZBR3gvbYFGG1K/RHJVLXKuceyEDUUcZpFDDQBO2EovJVtZzUmQYLFQWeObE/Q5SY/tXUgRLhQz8Fu+3FddCRmXBjoQplOFAfW0r7Y4M+TBcQM/eh5Qlt6yGxWmRgRCZPRG6CoeY60QR5uICLhA5MjKkbbA0Edswc6GW/jsiw1TF5PR0SRiGDLm2NP+ic62ogdLt/Bk1vbdooLiwoguOegChS5A2Rek763j6ODM6B/Vx6gBpp7q9dbKisKjtWUtjEkI3liYHU2odIB0xl0u0kC7WQEtURmGkEkyysEJ6FG33YlsahvkBU/ShPIrxNE/dQL93NqOl7808NuMHY4zBmMrZc4vFkEQi/2ZTRFweLbrStSkbt9jWnof6tWxKFZxfSpPOyXY2E03CLc3sWNaycH4t+CR7umb8GbgEQrdayeTzSnLTW3IttM+6/gws6pnDwaGlzcLDY2D+aGQWExmXnMZLobu3U
        OZpuaj8OWAivTtNeHHs25NuqT+jJnQ2UsbRV9ghM6oPFcf+DPMSzt6wF3q3mDzH+KdCVo0KNoUM32y3RHtoF3K1qRIDYJpJYdnnbd2GVyzPxZhlijLQBoiggE8lXYY5ujTe2LH3LZNqrcmG5s+EG+keXIFlGpkOifPJfC3YXeodzxZg1j3HvFSgC5WAQ3vHoULZRkISsdC5CmhFyqYpMHLaa+G1WuEsamIpM4OVTjXO+y5YuWGK3pBPdRJkZHyzTEnbaJUt6YjiakuRYZCCnfqAuySxugtVoSPyDEte75YjxkXJY2iMqflklI19cfdKh7k7lVPFvrUvT8QzzH5mduFJYxinFcBPz1JkGJpyeGbMMJizbbvkwzg1Mh5EdlNsjCidulScLUveiQxNFrtjKpnNx75xK+mg6aEjajNBCbjjwFNHZFEQdDsyQg0UbhNuET5k9xRb8YQaKOkROXl9S+w0nxa3G5DmireUkiZ2sXRDk7INJpx+QZF52K325brqNh16RXXKWSBnebSOOZ9GPMMD4IJPGXdJgdQvfMLeQm9vbR6rw3dJAJru1JKvNw9DbHoLm4Fl30ziv3mmmkn2z5dJHPaxFZwUnwsnW5RN00mTEe6b6dQM/zQYa02RE4eMVLMQ0QN8j0ATXhGSTpoES+Y0JDsEC3BlsnoiT0bUIBBBW+/GqOXTzmWOZ5w/jY4y4+lihIPdtC2amlzXeHLTvs+WZGF12gaqFY8mT6AylXLeZOLnlkNNZwzafJxii4fDLi5ReDNt6Pa98yZhGV7tNCvEWSLE7b0gwG0h46MssVDbdMgnMTJoBx2DK1tSLnJdqdRHj44TN4FGrFBoN4W5NirdYcgY+y1EEZXDkmedH35PWTnto8myovTWaaRIDy1B4LrH1ZExH2Uczh6PyhQwPBbahjcmyxwyQgcXVEOapOltchGaXX4cZEDWaoRbeEYJCJtnxSPj/oMUTN9cpoF+0BQd1IqK9EywdRJMazl0Cyc/MTLSlId0/5HUY/Mnl1fLIOeQQTzGl3fhkyHWfGJkTERzJq0SBbrCLEgUtCHneVOltFh09S7fgWfAwKA+7M1zoGPyDN5eeX1GFW/e/9QH662ucc4hA5PGM0wm2bBZz8H25BlNh1N6htSgMOFxyvXJjNBLCCXoylYUC0vUu3PN77X7HwYznLFrNQPGxngw2fr0jLwAMuVCZHRO1ovsUM+YkCa5LQtn03SnUQbbdTRZQ4a3TegJ91F4rAUBDClrPfbQtklswiSn5EypOO9aKHf7KlqmEyXD3UR2+7KSh1lygikgxuvqsrBA5O9/+I8WZYvxruQpt/Td+vGG2D722pRtUt7IwkUuQbNQVE+hHhtaeRtRk4bFebc2ipZsMPDVRHunwNvYx77ZniY+nokwzrc9xWakE5h+nu2vm3Y+mIxfKepT/gz+NbIA5wL9oA1Zxqo3NAktgnUjanVvLNxQqWc0WqwnlruZL38GpwqmPBklrl+rnCEyjuAdn/LCLUSGaGSeiepqmclQApPN6teSQwnSfxvjkzWbQkZNvTpI5DWe0nLGB7rx7l9fo1L4Yid8EkYMYmRg03sRzpZCJDd606mR0ZOVM9MmqZypRnOlcsbIYM9tzo4uQQaUhBU1DvKTIKPRqLSLSup/jcgJnpGKknr1s8kRV0cnQQa7beAZeyPj5hMY+nXmt2bUbABLnnvqg1PlyOYZFaTHoX5yZFRu8hHRpCQ0xzBjZCj32kUULy0+5DI5ToIMdJSVkGHXbiszrXE5lJnyCwoaSzso9ulK5oU8Q+dxGjFxZGkiDTKdGGrGb3o39KwXd4OMYvDw0RBrbkNzKO018PWVHcwnfUUa6M7EUm700kUK0xTCQ+4ik2xUjNh9Trmdg55
        RfHQs4kgaDEfV66MGMs4vmpRMnfjzaj/TUwnSgyZ4+nTRqZxHDMv9kaHcd2VwNY8iHXJdLPGaBNJNtomyNOqnc7Kdzeq00/KV5o1TGvSQ/DcaYRA3Qej6bB2mp6PuzbObp6szoibw+pcKzclV35lhFasjg+PuXsi9qjMKWVFdJERK48Gf0c6jEc0zk21svV1ZtWnWLKJ3bVcjzP6u2Aca5sIoZrgVGQor+GZ+bqyub9bvMEZsc6Jmz90snZrdHHkP7+1miYQCa8oH2hlYXgkZRu/sqRrfgFmnn2lRYmSEJutAxC0843jIuL4O0keWKIY6cqhcSKkU5dGmJKA6IG+ZyjkGhzkZ/f0S3vzRGEIGuSIycpBojAwZrunVszwjjEeg04bT8y794/AMiAgPq8qW7WK+KqMAqaFSXeXHFPVbV9BrONe0OzJUJiulNB88P2M+biLMMTULH46jJ0MBBSHDxhnxjPuW++nltyImfnqnQobyMwZNeeeVM1McSvHf5met8gqqu7hnFH6ViJqQMURPApdGvv6iSBNIXBvWfBOM+8wwCBekOXWX4l5dzzAdNuV0DcGwXXhG2ToI2lLEbXNaeuuhtA7hpRS48DRRv1LVKU16mrHuOeWgmp0YW73zjWdI4Cn0oo3o3Vw68BnGwLxcbEJu6tk32zOipg6780AncJPChIgkkk5U6RH/MerF/vZwUTVKVTsfKvM0YT18s33OqIW9RSk8lyw6Sw8A9JXWB8rfSvJrFOySmNTJ1o4QUZuRmv22ZSrHc6nbFOoH62yFQG7KpsZLRs97uvU8hnZzFrAujISIq0vvzWpy7fkdRdTk8vNECWu1nDBuUm+jUCvqBAcbGjW2SvTS3TymguEO3782C5SUE+iEJ5GaXcpIKpursNIOZAylMlrXOJTydexOFVGrkRHGD3uQIbjrDIHflDkDqkOTmH6Xqbcy6M4HGSxi6AdSbv0cz5hBBvupR8c55rlW29/3HwQ57h0rKvfOFDL2j9jVToKzQkZ4kmoSGZJb0jP86TzFkaUfNeCY0kCbUNmUBrpzRK3uEOV/ONS7wHwtyAj9S9nu70DYFM9QaRuNU1r5KpNVh7vpGckUKDxjRKtyHkdKqGmgDLo8QBsFNfSESJrUjc/qd50T6SqrMiw27Qk8hpPiw52FlFKBVLP3DCk2JU1YdJa+XtM2okZIOuQZU0VGdwbvVNyk7lDg9fxGOZXagnJsbG5nWrLLmakKjNS9yYYPUyZ71Be10cX3ysmwEU4EiZZOtnCdnVk1yAh93JCizkYIfKCbrPGKxLJa/Wg8Wzta3KRBJGDnnPtQsKxXpmB96ChODmGn2yqQI1kS7+rpok9LCa5SdfYMH3aqd36Xhks2ZbVuQYaqcnl/xnA0rX2X1zyOEzcJ3yuPXP/OVnowOgpnzlBW8JuR0lcJpl6E1W8EnWzKJh/4rJARUcnSulTQQU/AM5J3PKAv++CceYbcrITHyhGSLnwMIUeYB6RhjtUZlh1gYWcUABYCu/F8nxUyfBhSK15f7RMgAyU0vJSLCFMYMDwfniHg6uxyFyyWaCQ9HUJcXt0kVEoQnA8yWER/HomRs+j1+sb5GTBDz5NRqcJ8Se+ZD6WJbzajlNWNZzTQps/C0hAKxwcH3IKXKkOnc7IlSFHad052ysHo3+vZPIuYM+DHEbVkYWyeTUSt6Disa1jMT4czfXgwVJLXjZt4/WteleFfURoGcOwgFBaHbYGFri2Wl6+TJmHuuJ9sGFFbShO1r9K/RxnwcgfXLso2omZ87+5VVvLHzFYpk+si43DOHyaJQ4LVWq6QLoYFZEU7IYuu2KgeGWcSUWOEYbYf42fRtyMjzKJTnkFzaKJ/f4TE8qxlxSMYDFWxj5Sxp8pJ6
        /KPXHkdCW2ui2sr2yIBcfzJ8tIek5BBsoiD7bmhRj55W5nZEc+4vh6SHka7JzzUes7IYGzsVMQqEjQov72X+mk4A3D4DcnX4hU1Kz1rZMQl5K3CB4teW9oBMvgoOAifTj/6I3jnjIza80goHLUjvJG6x+ho26R7LssZjXWVKi9hpzJ36vf284w2nJZYaX1XUtFINjld2b5KTkB/xg0lC7bRKU16nHEHFb3S0i3xwgoR5WLIKFns8k0yXz/bqCQR8AJkHHiUX6j2089Ik3OIqIkg0Z1AbXKGdNULVmh0CuruFXOGW3rdzZfgnzpA5o9VNYel+DM8U8WHvmV4gGxLs5x1cU/1nYulDjsk0wc7y7JyFji/c9EV9hZ7ThFUDaDnZNiezVajyfU1e97Z87lAmV1dMH78GTXbYVGFMuM5/kSNZw9TqlATPVo3yNRK91SzDCHy0ovXK2tLXEQOAz4hewW+uNFPJ/gH24NNpmsuOOfI9OmqTpv1yfTFn1G4fS3XauVU/dTNtoYPpeHO+DMaEdPowuG1jDJZm5ahp8uQEdYjhka+rtmZ+EDHyMgVMHUyAFZn6tWwomUt0Rz5HKKw6nAR/ov2UH74E+gwZS5wlKMCstQpT5IjaKBVhYXJqnZn4gMNq8hXVfnGni6m523F4NBz0sabk0zaH40K1mM+meNkv9OL06qMLQ/OjOqKciujzAIDhVrzKGQgXsBm4iuY6/qBgvCV0cmr+xZk0ksZORChPbCDrOCvyTH2NNlzst6vFTrTtqp34enD6q6TDmRERVjNVIMK6dub41ZngoyBwdrAWNRRaCDpEzrSbvt7Uw9U+cC25Frv+hnmWHKGrWcaiNOgrAzXnVgUjbh2bUGcLTLCSx2Us9MjTVKb+w90r1ijpvnTrWeCjCJNdG4zTPvmQ5YTx5QJFztvl58wSbM5dkV7AKerLv0tJ8AOxSXvmaEIdb3Rz4FnQBlfM6icJGoUpjiiJoKFnMfXSpBmVz8zGmjd7BARteoqykknt7J1dHE3ENEthGjmxebkd3Ne3brin5CqqFyQgi0BIDBZJ44aXFJXAgezSOdpsu5kZzTQ5tU1J6i8nxviVBrCiGsYMvifBGf9sI1yFQ13NBaVja+UxvzePCCj6Y0/fTNe4ZvxYdMybMYrmmZAHpao9IhtXvDsFWb3S/eExyhpDRnBf1UVFMZb1NLKvz7UAoxsGaBDMrqm0EwkpEnnZH2zmv6Fhp4mpZlKuIaBUnk//fCARFDbT1ZWXFYx1TvTMTc9u6lCfGtlT1c6qJ3OXS4OifEVdyXnLp0o/ZOqGxLHNX9eebILc8dBT5X4PapIr8OVjb9O5n2ADLnAmFmYwtP42ENkHN9hjGjwSYq7oWTPb8FshmsNN7QJVZkVw4es5YxJyMIPqfCjmKISdhilD6FvQQa80ZdlYl/iByxu8tMiQ8RlnGvHzHbkGUosre1YjfC0yAA0kdZoupFueluMjKDI31AKrgiU0yKDtyMma9fFnjt+la/j56jq7Ji5e0JkSJRExR7tim45tZciww4Gpipp42uhUn1uqrIraHsSPSNxTqM4E6uqQSyu1LkKDoJOkldwuPDAKHTCiBrLxGKF9Q0YpNwwMTJCRZfuxAYJFljw2lko6Q5ju4bu0LZJPLwURMW2zLdJ7KR1HgoWw2AkdtPOsbN9K9pri2wT3h4dfjfXn8p4hsap6Rmhd1ye4CpXo606K502tN2P48+AXXH2MFdYWxA13UeBWPxdi8M99UG5Vj1zLZG54oSYiajVjopF/oypo9uqEKExhLrwHDLMZ3znOt0k1ebJlTqBR/WByhRM/wXvg3a8bg7f4uWf5z0sgC438VHZkId7AElqN0+/EA9reMpK1wB2RcbdK5yAPhu
        KftkQTRlzjf6QETXBwiIj+W69s5QjNVbkcj1J1UelXAyZBgPikw3BxtYV6LsjA1jF98AmPbRJHjwGMhLPyAcMzx4Wgkg+dO9u9js0z8CzMJSDbtkqRn59IHsXaSIeEFVpybcJeUZ3QJ4xOBYxScDl/tWSDq2E5v7TtTqp9PjoOSgy1Hm+U3K8hUAqC1rOEOwiTaT16JDCULpkU9kJg6WwjSNG1ExgY2sBeaVHHGl1d+dP5ttQcrVHRhMAW1EDRVKEZwAglxIyav035hltHuiQpFAnWrIYWOcuWHUpq71JydwnNdLnPE6li5qtdeeaKFGGrJJ4z8RIGQajk65I9DAVtH+yi3JjS7csjWerWjIkct1nk5uiPzdxkxq/HrzZ8e5Iz4ZId2VvHnl1ZsLBaqogU/PMhBKKEcQvdZoMXDrX+z0TWIi7pJKPoPYTH/9kmazUvfLMT7Y0qydbqCxqxKw6KTTVeo2UDL9exdPVAGAuojbifvcfhG6lZP9c1hcnFwFWVBBe6a2slRzG5gmVZFl0U+ZBZRA0wXRClg8VKczT5aOMB4uomZI+lIMd8VE+ZBGbOHBBRj3CLRG1ZjmHW+dbi79UvdRUj4uMYYz3La4G81harWtViJh7XpfBNuHWlbbBZkEaNxJ/1nrMxErZ2HI+89hQWho3aYBhFwcxZ38ORQGkUrHlNMhI9xqpkFI+brS7zribs+sSr4+u9sz8v6L+MZGhqkl+mVQ9pj/vsFuapDuAMxgbiZ5O85GcLSj1I8MDv4fBTjjT8h0lOujMeRn4ebJcEjvVz1onnjcdGobEomFXuGXtOnFiOjmDepNHfWhpMtDE3kiyqr/1iHEqDhpqLXM8o9N8ogs4RI5WjDalHetQSPdwGmiPUqa0I8bAYAArOiAsNN9nsJqKamjTISUVcML6ECakFYZK1VJ1u2eyXgMdgs8t20uZwHYbQVVKezPMMIIzFzdpJiO4sSOrfIiNz1XV1/OZDkcbT5d1g0xuPbRfLTkUPsdxeJZQfuKMkh1ljZ31U0UvfkiHsRvEnHOzc7KNccG39vdnDM6FFhksmS476zcJt0TUan01IcMojhchRVJa/8EQZguiR95V2i/w/BbsCzJli4BdAgtBBUH5IikQcNSpv9pMA1zKjEyXVMZ9Ke6mM4yQHqmBglkJDs137gzOwSebvMNh8Ezu+XI+sV/jWYoMW6bqxqU2Ok/uUHib3OrI8DqKB1CdL8N2tBLjKbGD8bO0OEIAiq460KFFcYLyo+qiJDcglXRqjdLbYshKtpgKHzZjs5D1fgfy5mOtUs5wnOQoYys0LfuwhPT6c5WXIiNvDq6KCT3TSjr1nuCTI0MDYM0SHTf7WwwWBgDt8IuUHxOL1Wklwa4zSHRkZDAwhprTuZ2ijZqFGlS2zeGQkV8BfWHCYUxLRZ+sXZVZfz7I8KyFuUhvrR9vJfFFv+Rh+PD4yIDg7UZNnEOysl6IdZARaqCFskiN6JykiWpE8nBCPDf32laPBqoFC/WMemxTFkFjJU01UwJb80who2k2hYymWc9kOzVQ7bGaJpDal4qXh74UoixscoEGytx2e6rEqkoVzdzMjp8nheN8rzfYbdZn9S3gO+T3u8S25GRSpuduYx7dsFeQ3rlTEcaIt2ynjN0biqeooG5YYrYfvH5DdwaZwp1aFI75yXoTv9mp2rU63dU8nskdYrKMEPJGF2XmdCE5ERpu3c+9+n2gRoVR0CUlk+U7gwP3gDnEcCRocD4Lst988mbIDDLKCPnWPhaBdwtOIaNRoZxnxXCy+mR5KYTFbpooS2f2CFXYfTpmP012R0YReFh0w/H7IJEYy9A40Pic5yGI5QHUTwW/y88bGXaeY+bAJqJc1ZH2UfxXQAYjyKOM3
        M+WWZ+kXeMOWn0bPZeQYeeA0uGiIFkJ9YLlmDKz+3fLKsgwNXO4REhso72jqzohLojsxWA7bcWwmWcGIbG8mzXUvTrDhytF1LIbBmL6qvIKGSLBWYhyW/MKPKPRoXo0UH2l8GG0oRS7Cus4W7xNN5eWKMMhlLIeW9Hbz+Fkz1YDxXU7IbvNnd+cYPDa8QINFEqFsW8Pt1DvK8145XCZWZyPiZMDsJdD9P1sLfRn+OH5Zn6nhpP1co1mfrJyiDVPyJb22akzk+VdENBcFzmpoA2b6b4ECEuKhlijX9lwsjueKihm3laLgFVH8Zk5I6Qb2LVgJ0HG4B0f0V+erno5+TNEhm/WI9f2tJIk13gRpBtom4T1WKsDFhC/3gxTVpKfxV7ICLeRdxgDDrz07gjC5iwC/7Q56rnH4Rw/vZBndFKhp9nJvOOpHjJEmzIAUS9AjJ05qy4bkNCc8qw02+AYyBAHQ3OeBoclF5HZ0Kkezmg8zfSetci4c42tYZZImHmUYhEQXMKuhvjZIUNLTownm1VRjgyWFV4aBTPr4GeYdhaK3ucAz0gVSO9c4y0Ms/ekbUBkxS/rlIOiAByDZ4Si1zNYmhVmMIAjTLg1+4of/HSDq8P+XxZBzQxW5xmeWKGe0RNrrSdbd9vvWZmebL5fAXMjyq3MJC2wOAgykGH1caXyu6W6jB+W3LecaYbfIrPB6XrvWLM6yMRK0HnzRv2pfyoPfzIS37KzmZ8Cve0/2Xp4fmyedHbGzj93r9QPBNFtXynPISoDkbiFPFozpGsmO0W6EAC7R9Qaf0bZNCX2LQOEVOa58mo3n0B7grtY41S2stnTYYh8ykp6hvsz7LyuuCYEma1JZ6ffyDST2aJZT9nPPTTxzhs+WcUHOlrNxovAn+QnBknFhZGkeDE7I0wT7DQpQ7m2j2F8irhJrjw2hK8nj73oVIslHValj1f3rBwcGbLFMbVHFficmq37TrOf1JCWS8E/B5CxyS7Gv6mCxlMn50Qlf9XhDM9o3G79u+UYyJD5pEI5E+7zXLUZ2Un1reFSCNtDzwFk2D5gyuQqJ2tuklUo612HAzp9bvt4Y4+EDCkQyjSJMn1GNdLZFsYtB5u2x0G5ujTxETWoHNrPDfWnvAi+mWjCiyRtBxskLjumA4lDCvi94yGj0VM6I2ohsYoGWvdZaMp6c/R0fnNgpivNP5ktlougroq2tWdOV89kDxdR0yxEYSZIQnU+ZTnJLXJtljqXvZnCQTRQhgihwW/9SEluHpa8acafS5uxRRQZwiSbsdSV9wyA4LGJf+ZSOxqA7Mzm8WMLm3VOtrNZJ01ke+uR/4bQl+76m9YqjHnwr7ATyCWT3qzrdNqlc7KdzcLJ9p432cHTVfPeVg9K55J1BGi+Vo6EK35AyVc90KhwkRl/X+gWnDKMt4YS+qX7vKeLiTAdJjW/MVSBDxLphsAyPHXu5drJfKCdETVZIn7cXsraibE71xjuuWLO7EFT4QOHuupS+FByKN0PjYypyUaeTat2wuCZQkcRB8u0oJnuam+8AKLksxYZzK2oMuyJDllrQWfwgQzC3Me6s1s5El8tmOsk1oo8YysyhH6GyoAZ/+DTnK/PYayCmZbaLJ268LOEZ8g9WqulRAu3Cd1UcD6VJ4B2uu4K8kn5qlTmUdUKz9gPhYwqBZrBaJ0YHoPUUemsT8SVGnItfbFGSFErm+eOjE49o1OaNMgQRPCBqpyBL2YdWfkWk9Nld+QuwHi8wdK5jUKXiQfQrJ6x8VbxRYpkMiRVcOu8vQsXFlhH1gyO4FwtuShVjTjuZJC+GVTq3C0XvNJrsHwihl8e6Rm+ZdNM26Vpxvi8KSGeUbcUFTiLJ6ukOjQbh5SUilxYCCtBjIZtWh18Mou
        3mYUtebp8dbAULFoRTtZSVvWTHLKeJrIRRH1eyqsZAMNg3yMfZ5nERqBo/PxXBZlDmngi+2ZSR/xkGwqHkw0BcMCIWujPKCgWeJtHhIaXIpgh7lAup6twlorgsE2lq7JrWSpkfKMRF9WkvHosiXIxjNqfof1aNbNJ0C0n6HkFL5KtwavDAllTbs1SnYGYQMGEp0ln+DBU/H1vU95x7+M5kg80ZGveYDEqDAKbcBF2POBYRO6akcB4FKzBt4afEbcSRSaogcGKqgiCRfNT4DscnuwCmtGYr/BFvk4ndEWHdEvnqsbUITIqcCdViS8yLyZYKkOKM3lWP+PWq03ZHpMwFOLi6M1a8Ml5ISPxjCKzTZcUPkRK5z/2Uma4FqnKrlbyiwqnsLNV2Q0Dh41OoTfyDjEOUfrIvyWVkh9+4U8+5J9oQDMa8xVxBa1oW9Vpe3m4XEiOL0obrTAhWNqsHyLDKBH6JFq9L/EPtjW7lrp0yIg+nW679FGlJeWVyWZufsQJhICZ4GdfOVFDMB3KZcdEpqqZPURGNzLSNtKZaXkvUN3R8ko5ttUKOe5Y0G0rBLMBBewYtu4gkpMbqdFpP59YmtTircR7Gukrn/Txzaf6paIssh/VT5dyV1Jm6zodvsFQPkTyC0OUQdrpII26EucnQQYvndFAa63F9AyFuPwTJjMeuplpfO4JhycTGsUQYw/qI2VYhqyrrlb3cxGSjD0wAOkiDAmpwfB054ZU3WYinTPtp0lnh53NThNRmwFvzZO2cq/MQq7NfIBXoz8iyAnEqEDsRk90RY8n7rsoKm05BCaVtlJsqyRF2cm8iNfxUl7NAJB3UiMKk1hrsjVljuHp6mRr8nT5AXn5srr55F86L9cwZ1genAR4pvE7sWYyKGRTJLiUm11rBIw5xCAXhCHcbvxIVy02DsjgFbheTIG4RVHp/DQCd5GtGE7Wd/gQGXMV3BruUmy/qr6P+UBxdrFy8Hb8ENiiGDgsMHDBG8GP7FiJAP3odz5UA35ozFdgCXydTuiKDpEUyfdVwjQbB7nfLf3Zlw+Rse+JZ+8RqpCRyau4iRzGJlxVKXZ42OWqAQqDIdLR/PAh/2R1xipmIBkhMSF1MjPIfEA3o+QhMozGZyJNGMbODFYJhc3jC4v1T/YhMp6RyOj0IvTEWrfmZxR590yUJv8PVSfAJKcz9W4AAAAASUVORK5CYII=';
        $usuario->save();

        return view('welcome');
    }

    public function top(){
        if($this->testLogin()){
            return redirect()->route('welcome');
        }
        $user = usuario::where('id', session("key"))->get()[0];

        $usuarios = DB::table('usuarios')
            ->leftJoin('hilos','usuarios.id', '=', 'hilos.userCreator')
            ->selectRaw('count(hilos.id) as numH, usuarios.id, usuarios.nomUsuario, usuarios.created_at, usuarios.img')
            ->groupBy('usuarios.id')
            ->orderby('numH', 'desc')
            ->get();

        return view('usersTop')->with('usuario', $user)->with('usuarios',$usuarios);
    }

    public function topshow($username){
        if($this->testLogin()){
            return redirect()->route('welcome');
        }
        $user = usuario::where('id', session("key"))->get()[0];

        $usuario = DB::table('usuarios')
            ->select('id', 'nomUsuario','descripcion', 'correo','created_at', 'img')
            ->where('nomUsuario', '=', $username)
            ->get()[0];
        
            $usuario->created_at = substr($usuario->created_at,0,10);
        
        $hilosCreated = DB::table('hilos')
                ->Join('usuarios','hilos.userCreator', '=', 'usuarios.id')
                ->leftJoin('likes','hilos.id', '=', 'likes.id_Hilo')
                ->where('usuarios.nomUsuario','=', $username)
                ->selectRaw('hilos.*, usuarios.nomUsuario, usuarios.img, count(likes.id) as likes')
                ->orderBy('hilos.id', "asc")
                ->groupBy('hilos.id')
                ->get();

        $friend = DB::table('amigos')->where([
            ['userPrinc', '=', $user->id],
            ['userFriend', '=', $usuario->id],
            ])->get();

        return view('userpublic')->with('usuario', $user)->with('user',$usuario)->with('hilos',$hilosCreated)->with('amigo',$friend);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        if($this->testLogin()){
            return redirect()->route('welcome');
        }
        $user = usuario::where('id', session("key"))->get()[0];
        
        
        $hilosCreated = DB::table('hilos')
            ->Join('usuarios','hilos.userCreator', '=', 'usuarios.id')
            ->leftJoin('likes','hilos.id', '=', 'likes.id_Hilo')
            ->where('usuarios.nomUsuario','=', $user->nomUsuario)
            ->selectRaw('hilos.*, usuarios.nomUsuario,  count(likes.id) as likes')
            ->orderBy('hilos.created_at', "desc")
            ->groupBy('hilos.id')
            ->get();


        return view('userEdit')->with('usuario',$user)->with('hilos',$hilosCreated);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        if($this->testLogin()){
            return redirect()->route('welcome');
        }

        $user = usuario::where('id', session("key"))->get()[0];

        $user = usuario::find($user->id);

        $user->nombre = $request->input("nombre");
        $user->apellido = $request->input("apellido");
        $user->fechaNac = $request->input("fechaNac");
        $user->descripcion = $request->input("descripcion");
        if($request->input("password") == null){
            $user->contraseña = $user->contraseña;
        }else {
            $user->contraseña = $request->input("password");
            $user->save();
        }
        return $this->edit();
    }

    public function updateIMG(Request $request){
        if($this->testLogin()){
            return redirect()->route('welcome');
        }

        $user = usuario::where('id', session("key"))->get()[0];

        $user = usuario::find($user->id);

        $user->img = $request->input("fileUP");

        $user->save();
        return redirect()->route('editpage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroyFromAdmin($id){

        if($this->testLogin()){
            return redirect()->route('welcome');
        }

        $user = usuario::where('id', session("key"))->get()[0];

        if($user->id != 1){
            return redirect()->route('welcome');
        }


        usuario::destroy($id);
        
        return redirect()->route('listaUser');
    }

    public function addFriend(Request $data){
        
        if($this->testLogin()){
            return redirect()->route('welcome');
        }

        $user = usuario::where('id', session("key"))->get()[0];

        if( $user->id == $data->id){
            return;
        }

        $userFriend = usuario::where('id',$data->id)->get()[0];


        $friend = new amigo;
        $friend->userPrinc  = $user->id;
        $friend->userFriend  = $userFriend->id;

        $friend->save();
        
        $friend = new amigo;
        $friend->userPrinc  = $userFriend->id;
        $friend->userFriend  = $user->id;

        $friend->save();

        return 202;
    }

    public function deleteFriend(Request $data){
        if($this->testLogin()){
            return redirect()->route('welcome');
        }

        $user = usuario::where('id', session("key"))->get()[0];

        $delFriend1 = DB::table('amigos')->where([
            ['userPrinc', '=', $user->id],
            ['userFriend', '=', $data->id],
        ])->get()[0];

        $delFriend2 = DB::table('amigos')->where([
            ['userPrinc', '=', $data->id],
            ['userFriend', '=', $user->id],
        ])->get()[0];

        DB::table('amigos')->where('id', '=', $delFriend1->id)->delete();
        DB::table('amigos')->where('id', '=', $delFriend2->id)->delete();


    }


    private function testLogin(){
        $userlog = session('key');


        if(strcmp($userlog, "") == 0){
            return true;
        } else {
            return false;
        }
    }
}
