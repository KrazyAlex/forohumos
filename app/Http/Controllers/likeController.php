<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\like;
use App\hilo;
use App\usuario;

use Illuminate\Support\Facades\DB;

class likeController extends Controller
{
    public function addLike($id){
        if($this->testLogin()){
            abort(404);
        }
        $user = usuario::where('id', session("key"))->get()[0];

        $newLike = new like;
        $newLike->id_Hilo = $id;
        $newLike->id_Usuario = $user->id;
        $newLike->save();


        $likes = DB::table('likes')
        ->where('id_Hilo' , $id)
        ->selectRaw('count(*) as numLikes')
        ->get()[0];

        return $likes->numLikes;
    }

    public function dislike($id){
        if($this->testLogin()){
            abort(404);
        }
        $user = usuario::where('id', session("key"))->get()[0];

        $likeToDel = DB::table('likes')
        ->whereRaw('id_Hilo = '.$id.' AND id_Usuario = '.$user->id)
        ->select('id')
        ->get();

        DB::table('likes')->where('id', '=', $likeToDel[0]->id)->delete();

        $likes = DB::table('likes')
        ->where('id_Hilo' , $id)
        ->selectRaw('count(*) as numLikes')
        ->get()[0];

        return $likes->numLikes;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function testLogin(){
        $userlog = session('key');


        if(strcmp($userlog, "") == 0){
            return true;
        } else {
            return false;
        }
    }
}
