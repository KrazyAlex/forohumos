<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\usuario;
use App\hilo;
use App\etiqueta;

use Illuminate\Support\Facades\DB;

class hiloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($this->testLogin()){
            return redirect()->route('welcome');
        }
        $user = usuario::where('id', session("key"))->get()[0];

        $hilos = DB::table('hilos')
                    ->Join('usuarios','hilos.userCreator', '=', 'usuarios.id')
                    ->leftJoin('likes','hilos.id', '=', 'likes.id_Hilo')
                    ->selectRaw('hilos.*, usuarios.nomUsuario, usuarios.img, count(likes.id) as likes')
                    ->orderBy('hilos.created_at', "desc")
                    ->groupBy('hilos.id')
                    ->get();
        return view('index')->with('usuario',$user)->with('hilos',$hilos);
    }

    public function searchview(){
        if($this->testLogin()){
            return redirect()->route('welcome');
        }
        

        $user = usuario::where('id', session("key"))->get()[0];
        $hilos = [];
        return view('search')->with('usuario',$user)->with('hilos',$hilos);
    }

    public function search(Request $request){
        if($this->testLogin()){
            return redirect()->route('welcome');
        }
        $user = usuario::where('id', session("key"))->get()[0];

        $query = $request->input("text");

        $hilos = DB::table('hilos')
            ->Join('usuarios','hilos.userCreator', '=', 'usuarios.id')
            ->leftJoin('likes','hilos.id', '=', 'likes.id_Hilo')
            ->whereRaw('upper(tema) LIKE upper(\'%'.$query.'%\')')
            ->selectRaw('hilos.*, usuarios.nomUsuario, usuarios.img, count(likes.id) as likes')
            ->orderBy('likes', "desc")
            ->groupBy('hilos.id')
            ->get();

        return view('search')->with('usuario',$user)->with('hilos',$hilos);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createPage(){
        if($this->testLogin()){
            return redirect()->route('welcome');
        }
        $user = usuario::where('id', session("key"))->get()[0];

        $etiquetas = etiqueta::all();

        return view('hilocreate')->with('etiquetas',$etiquetas)->with('usuario',$user);
    }

    public function create(Request $request)
    {

        if($this->testLogin()){
            return redirect()->route('welcome');
        }
        $user = usuario::where('id', session("key"))->get()[0];

        $hilo = new hilo;

        $hilo->tema = $request->input("tema");
        $hilo->texto = $request->input("texto");
        $hilo->etiquetaid = $request->input("etiqueta");
        $hilo->userCreator = $user->id;
        $hilo->save();

        return redirect()->route('mainpage');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($this->testLogin()){
            return redirect()->route('welcome');
        }
        $user = usuario::where('id', session("key"))->get()[0];

        $hilo = DB::table('hilos')
        ->join('etiquetas','hilos.etiquetaid', '=', 'etiquetas.id')
        ->Join('usuarios','hilos.userCreator', '=', 'usuarios.id')
        ->leftJoin('likes','hilos.id', '=', 'likes.id_Hilo')
        ->where('hilos.id','=',$id)
        ->selectRaw('hilos.*,etiquetas.nombre as nomEtiqueta, usuarios.nomUsuario, usuarios.img, count(likes.id) as likes')
        ->groupBy('hilos.id')
        ->get()[0];
        

        $comentarios = DB::table('comentarios')
        ->select('*')
        ->where('idHilo','=',$hilo->id)
        ->orderBy('id', "asc")
        ->get();

        $liked = DB::table('likes')
        ->Join('usuarios','likes.id_Usuario', '=', 'usuarios.id')
        ->Join('hilos', 'likes.id_Hilo', '=', 'hilos.id')
        ->selectRaw('count(*) as liked')
        ->where([
            ['usuarios.id', '=', $user->id],
            ['hilos.id', '=', $hilo->id]
        ])
        ->get()[0];

        return view('hilo')->with('usuario',$user)->with('hilo',$hilo)->with('comentarios', json_encode($comentarios))->with('liked', $liked->liked);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if($this->testLogin()){
            return redirect()->route('welcome');
        }

        $user = usuario::where('id', session("key"))->get()[0];

        $hilotoDelete = hilo::where('id', $id)->get();

        echo '<script>console.log(\''.$hilotoDelete.'\')</script>';

        hilo::destroy($id);

        return redirect()->route('mainpage');

    }

    private function testLogin(){
        $userlog = session('key');


        if(strcmp($userlog, "") == 0){
            return true;
        } else {
            return false;
        }
    }
}
