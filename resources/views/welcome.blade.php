<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/ico" href="/logIco.ico" />
        <title>Foro Humos</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            body{
                background-image:url('/Back.png');
                background-repeat: repeat;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: white;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .white{
                color: white;
            }

            .blue{
                color: blue;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            #btnContainer{
                width: fit-content;
                margin: auto;
                background-color: #343a40;
                padding: 25px;
                border-radius: 12px 12px 12px 12px;
                -moz-border-radius: 12px 12px 12px 12px;
                -webkit-border-radius: 12px 12px 12px 12px;
                border: 1px solid #000000;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Foro Humos
                </div>
                <div class="links"> 
                    <div id="btnContainer">
                        <button onclick="window.location.href = '/register'" type="button" class="btn btn-outline-light"><a class="blue">Registrarse</a></button>
                        <button onclick="window.location.href = '/login'" type="button" class="btn btn-outline-warning"><a class="white">Conectarse</a></button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
