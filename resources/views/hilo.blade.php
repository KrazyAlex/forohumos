@extends('master')

@section('script')

    <script defer src="../../js/app.js"></script>
    <script>

        class ComentarioMaster {
            constructor(data) {
                this.data = data;
                this.coments = [];
            }
        }

        function buscarCom(list, datatofind){
            list.forEach(data => {
                if(data.data.id == datatofind.data.idComent){
                    data.coments.push(datatofind);
                } else if(data.coments.length > 0){
                    buscarCom(data.coments, datatofind);
                }
            });
        }

        window.onload = function () {
            var listaCom = '{{$comentarios}}';

            console.log(listaCom);
            listaCom = listaCom.replace(/&quot;/gi,'"');
            listaCom = JSON.parse(listaCom);
            console.log(listaCom);

            var cont = document.getElementById('comentarios');

            var listaComentariosMaster = [];

            listaCom.forEach( element => {
                var newcom = new ComentarioMaster(element);

                if(element.idComent == null){
                    listaComentariosMaster.push(newcom);
                }else {
                    buscarCom(listaComentariosMaster, newcom);
                }

            });

            console.log(listaComentariosMaster);

            cont.innerHTML += createModnvl1(listaComentariosMaster);

            console.log(listaCom);
        }


        function createComM(params, nvl) {
            if (nvl > 3) {
                
            } else {
                return '<div class="row comRow"><div id="comentario'+params.id+'" class="col-lg-12 bg-dark" style=" color: white;"><div style="height: 60px"><div class="container"><div class="row comRow"><div class="col-md-1">#'+params.id+'</div><div class="col-md-11">'+params.texto+'</div> </div> </div></div><div><p><a  style="color:black;" onclick="openclose(\''+params.id+'\')" class="btn btn-light" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">Responder</a> </p><div class="collapse" id="collapse'+params.id+'" style="padding-botton: 10px;"><div class="card card-body" style="padding: 0.5rem;"><form action="" method="post"><textarea style="resize: none;" rows="2" cols="50" name="comment"></textarea><a type="button" class="btn btn-dark">Comentar </a></form></div> </div></div></div>';
            }
        }

        function createModnvl1(lista){
            var data = '<div class="container">';

            lista.forEach(elemento => {
                data += '<div class="row comRow"> <div id="comentario'+elemento.data.id+'" class="col-lg-12 bg-dark" style=" color: white;"><div style="height: 60px">';

                data += '<div><i>H/'+elemento.data.id+'</i></div><p>'+elemento.data.texto + ' </p></div><div><p>';

                data += '<a  style="color:black;" onclick="openclose('+elemento.data.id+')" class="btn btn-light" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">Responder</a>';
                
                data += ' </p><div class="collapse" id="collapse'+elemento.data.id+'"><div class="card card-body" style="padding: 0.5rem;">';

                data += '<form action="/createCom/'+elemento.data.idHilo+'/'+elemento.data.id+'" method="post"> @csrf<textarea required style="resize: none;" rows="2" cols="50" name="comment"></textarea><button type="submit" class="btn btn-dark">Comentar </button></form></div></div></div>';

                data += ' <div id="comentariosXcom'+elemento.data.id+'"class="container">';

                data += createModnvl2(elemento.coments);

                data += '</div>';


            });

            data += '</div></div>';

            return data;
        }

        function createModnvl2(lista){
            var data = "";
            lista.forEach(element => {
                data += '<div class="row comRow"> <div class="col-lg-11 offset-1" style="background-color: whitesmoke; color: black;"><div >';

                data += '<div><i>H/'+element.data.id+'</i></div><p>' + element.data.texto;

                data += ' </p></div><div><p><a  style="color:white;" onclick="openclose('+element.data.id+')" class="btn btn-dark" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">';
                    
                data += 'Responder</a> </p><div class="collapse" id="collapse'+element.data.id+'"><div class="card card-body" style="padding: 0.5rem;">'
                    
                data += '<form action="/createCom/'+element.data.idHilo+'/'+element.data.id+'" method="post"> @csrf<textarea required style="resize: none;" rows="2" cols="50" name="comment"></textarea><button type="submit" class="btn btn-dark">Comentar </button></form></div></div></div>';
                
                data += '<div id="comentariosXcom'+element.data.id+'"class="container">'

                data += createModnvl3(element.coments, 0);

                data += '</div>';

            });
            data+= '</div></div>';
            return data;
        }

        function createModnvl3(element, type){
            var data = "";
            element.forEach( intercom => {

                data += '<div class="row comRow"> <div class="col-lg-11 offset-1" style="background-color: #333333; color: white;"><div >';

                data += '<div><i>H/'+intercom.data.id+'</i></div><p>' + intercom.data.texto;

                data += ' </p></div><div><p><a  style="color:black;" onclick="openclose('+intercom.data.id+')" class="btn btn-light" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">';
                    
                data += 'Responder</a> </p><div class="collapse" id="collapse'+intercom.data.id+'"><div class="card card-body" style="padding: 0.5rem;">'
                    
                data += '<form action="/createCom/'+intercom.data.idHilo+'/'+intercom.data.id+'" method="post"> @csrf<textarea required style="resize: none;" rows="2" cols="50" name="comment"></textarea><button type="submit" class="btn btn-dark">Comentar </button></form></div></div></div>';
                
                data += '</div></div>';
                
                if(intercom.coments.length > 0){
                    data += createModnvl3(intercom.coments, 1);
                }

            } );
            if(type == 0){
                data += '</div></div>';
            }
            return data;
        }

        function openclose(id){
            var item = document.getElementById('collapse'+id);
            console.log(item);
            const input = item.children[0].children[0][1];
            console.log(input);
            if(item.style.display== "block"){
                item.style.display= "none";
                input.value = " ";
            }else {
                item.style.paddingBottom = "10px";
                item.style.display= "block";
            }
            
       } 

       //  <a href="javascript: history.go(-1)">Volver</a>
    </script>
    <script>
        function getLiked(){
            return {{$liked}};
        }
        function getId(){
            return {{$hilo->id}}
        }
    </script>
@endsection

@section('style')
    <style>
        #hiloTitle{
            text-align:center;
            padding-bottom: 12px;
        }
        .comRow{
            margin-bottom: 10px;
        }
        #hiloText{
            background-color: #e9ecef;
            height: 160px;
            padding-top: 20px;
        }
        .hRow{
            margin:0;
        }
        #likeBTN{
            cursor: pointer;
        }
        .etiqueta{
            background-color: #ffb62c;
            width: fit-content;
            padding: 12px;
            border-radius: 12px;
            cursor: context-menu;
        }
        .likeOrange, .likeGray {
            height: 70px;
            width: 125px;
            background: center;
            margin: auto;
        }
        .likeOrange {
            background-image: url('/like_orange.ico');
        }
        .likeGray {
            background-image: url('/like_gray.ico');
        }

    </style>
@endsection

@section('content')
    <h4 id="hiloTitle">{{$hilo->tema}}</h4>
    <div class="container-fluid">
        <div class="row hRow">
            <div class="col-12" id="hiloText">
                <p>{{$hilo->texto}}</p>
            </div>
        </div>
        <div class="row hRow">
            <div class="col-12 col-lg-10">
                <p>Etiquetas del Hilo:</p>
                <p class="etiqueta">{{$hilo->nomEtiqueta}}</p>
            </div>
            <div id="likeDiv" class="col-5 col-sm-4 col-lg-2">
                <p>Nº Likes: <span id="numLikes">{{$hilo->likes}}</span></p>
                <div id="likeShow"></div>
            </div>
        </div>
    </div>


    <div>
        <div style="text-align: center">Caja de comentarios</div>
        <hr>
        <br>

        <div class="card card-body" style="padding: 0.5rem;">
            <form action="/createCom/{{$hilo->id}}/0" method="post">
                @csrf
                <textarea required style="resize: none; width: 100%"" rows="2" name="comment"></textarea>
                <div style="text-align: center">
                    <button type="submit" class="btn btn-dark">Comentar </button>
                </div>
               
            </form>
        </div>
        <br>
        <hr>
        <br>
    </div>
    <div id="comentarios" style="padding: 5px 15px 5px 34px;">
    </div>
    
   
@endsection
