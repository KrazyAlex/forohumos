<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
  <link rel="icon" type="image/ico" href="/logIco.ico" />
    <title>Register</title>
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        body{
            background-image:url('/Back.png');
            background-repeat: repeat;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: white;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .white{
            color: white;
        }

        .blue{
            color: blue;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        #formulario_login{
            background-color: #343a40;
            padding: 25px;
            border-radius: 12px 12px 12px 12px;
            -moz-border-radius: 12px 12px 12px 12px;
            -webkit-border-radius: 12px 12px 12px 12px;
            border: 1px solid #000000;
        }
        
        h1{
            color: #e9ecef;
        }
    </style>
    <script>
        
        function testPaswd(){
            console.log('hi');
            var password = document.getElementById("inputContraseña");
            var password2 = document.getElementById("inputRepContraseña");

            if(password.value === password2.value){
                password2.setCustomValidity('');
                return true;
            }else{
                password2.setCustomValidity('Las contraseñas no coinciden.');
                return false;
            }   
        }
        
    </script>
</head>
<body>
<div class="flex-center position-ref full-height">
    <form id="formulario_login" method="POST"  onsubmit="return testPaswd()">
        @csrf
        <h1 class="h3 mb-3 font-weight-normal">Registro</h1>
        <label for="inputNombre" class="sr-only">Nombre</label>
        <input name="nombre" required type="text" id="inputNombre" class="form-control" placeholder="Nombre">
        <br>
        <label for="inputApellido" class="sr-only">Apellido</label>
        <input name="apellido" required type="text" id="inputApellido" class="form-control" placeholder="Apellido">
        <br>
        <label for="inputFechaNac" class="sr-only">Fecha Nacimiento</label>
        <input name="fechaNac" required type="date" id="inputFechaNac" class="form-control" placeholder="DD/MM/YYYY">
        <br>
        <label for="inputMail" class="sr-only">Correo Electronico</label>
        <input name="correo" required type="email" id="inputMail" class="form-control" placeholder="Correo Electronico">
        <br>
        <label for="inputUsuario" class="sr-only">Usuario</label>
        <input name="username" required type="text" id="inputUsuario" class="form-control" placeholder="Usuario">
        <br>
        <label for="inputContraseña" class="sr-only">Contraseña</label>
        <input name="password" required type="password" id="inputContraseña" class="form-control" placeholder="Contraseña">
        <br>
        <label for="inputRepContraseña" class="sr-only">Repetir Contraseña</label>
        <input name="password2" required type="password" id="inputRepContraseña" class="form-control" placeholder="Repetir Contraseña">
        <br>
        <button class="btn btn-lg btn-outline-warning btn-block" type="submit" >Enviar</button>
        <a href="/" class="btn btn-s btn-outline-light btn-block" >Volver</a>
    </form>
    
</div>
</body>
</html>