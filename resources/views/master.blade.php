<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Foro Humos</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  
  <link rel="icon" type="image/ico" href="/logIco.ico" />
</head>

<script defer src="js/app.js"></script>

<script>
  function redirect() {
    window.location.href = "/userPage";
  }
  function logout() {
    window.location.href = "/";
  }
  function getUsername(){
    return '{{ $usuario->nomUsuario }}';
  }
</script>
@yield('script')
<style>
  body{
    min-height: 100%;
  }
  a{
    font-size: 20px;
  }
  .jumbotron {
    height: 10rem;
    margin-bottom: 0px;
    background-image:url('/Back.png');
    background-repeat: repeat;
    padding-top: 30px;
  }

  #tittle {
    text-align: center;
    font-size: 4.5rem;
  }

  

  #chatBox {
    border: 5px solid #343a40;
    background-color: #ffb62c;
    border-radius: 10px;
    height: 658px;
    width: 100%;
    margin: 20px 0 0 15px;
    overflow: auto;
  }

  .friendName{
    height: 40px;
    flex: auto;
    padding: 3% 15px;

    background-color: #343a40;
    margin: 10px;
    margin-bottom: 0px;
    color: white;
  }

  .chatResponser{
    flex: auto;
    background-color: #e9ecef;
    height: 150px;
    margin:0 10px;
    overflow: auto;
  }
  .msgResponse{
    margin: 1px 0;
    max-width: fit-content;
    max-width: -moz-max-content;
  }
  .msgD{
    flex-direction: row;
  }
  .msgI{
    flex-direction: row-reverse;
  }
  .msgTO{
    background-color: #ffc107;
  }
  .msgFROM{
    background-color: #343a40;
    color: white;
  }

  #msgTEXT{
    width: 80%;
  }

  .chatResponserForm{
    flex: auto;
    background-color: #343a40;
    height: 40px;
    margin:0 10px; 
    padding-top: 4px;
  }

  .invisible {
    display:none;
  }

  #msgToINV{
    display:none;
  }

  #addMSJ{
    right: 15px;
    position: absolute;
    font-size: 40px;
    top: -15px;
    cursor: pointer;
  }

  #footer-Style{
    position: relative;
    background-color: #343a40;
    color: white;
    bottom: 0px;
  }

  .hbtn{
      background-color: #343a40;
      border-color: #343a40;
  }

  #errorControl{
      color:red;
      display:hidden;
  }
  .userIMG{
      height:37px;
      width:37px;
      border-radius: 200px 200px 200px 200px;
      -moz-border-radius: 200px 200px 200px 200px;
      -webkit-border-radius: 200px 200px 200px 200px;
      border: 1.5px solid #000000;
  }
  
  #btnCreate{
    float:right;
    margin-right: 53px;
  }
  table tbody tr:hover{
    background-color:rgba(52,58,64,0.9) !important;
    color:white;
    cursor: pointer;
  }
  #perfilLogOutBtns{
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }

  #colImgStyle{
    text-align: center;
  }

  #colImgStyle > img {
    height: 135px;
  }

  #footerCopy{
    color:#e9ecef;
    text-align: center;
    padding-bottom: 25px;
  }
  
  #likeDiv{
      text-align: center;
    }

  @media (max-width: 1000px) {

    #tittle {
      font-size: 3.5rem;
    }
    #chatBoxDiv {
      display:none;
    }
    
    #containerBoxDiv, #innerContainerBoxDiv{
      padding:0px;
    }
    #contentBoxDiv{
      padding-left:31px;
      padding-right:20px;

    }

    #personalIMG{
      text-align: center;
    }
    table{
        width:auto;
        font-size: 10px;
    }
    .btn{
        font-size: 10px;
    }
    
    #btnCreate{
      margin-right: 0px;
    }
    .perfilBtn, .logOutBtn{
        
        font-size: 1rem;
    }
    #perfilLogOutBtns{
        text-align: center;
        padding:0px;
    }
    .lastCol{
        display:none;
    }
    .creatorCol{
      text-align:center;
    }
    #likeDiv{
      text-align: left;
    }
  }
</style>
@yield('style')
<!--
    background-color: #343a40;
-->
<body>
  <div class="jumbotron">

    <h1 id="tittle" class="display-4">Foro Humos</h1>
  </div>
  <div class="container-fluid">
    <div class="row">
      
      <div class="col-lg-2"  id="chatBoxDiv">
        <div id="chatBox"></div>
      </div>
      <div class="col-12 col-lg-9" id="containerBoxDiv">
        <div class="container-fluid" id="innerContainerBoxDiv">
          <div class="row">
            <div class="col-lg-12">
              <nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                  aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                  <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li id="homeNav" class="nav-item">
                      <a class="nav-link" href="/mainPage">Home </a>
                    </li>
                    <li id="searchNav" class="nav-item">
                      <a class="nav-link" href="/search">Buscar Hilo</a>
                    </li>
                    <li id="userTop" class="nav-item">
                      <a class="nav-link" href="/userstop">Top Usuarios</a>
                    </li>
                    @if( $usuario->id === 1)
                    <li class="nav-item">
                      <a class="nav-link" href="/users">Gestor de Usuarios</a>
                    </li>
                    @endif
                  </ul>
                  <ul id="perfilLogOutBtns">
                    <li onclick="redirect()" class="btn perfilBtn btn-outline-warning my-2 my-sm-0">{{ $usuario->nomUsuario }}</li>
                    <li onclick="logout()" class="btn logOutBtn btn-outline-light my-2 my-sm-0">Cerrar Sesion</li>

                  </ul>
                </div>
              </nav>
            </div>
          </div>
          
          <br>
          <div class="row">
            <div class="col-lg-12" id="contentBoxDiv">

              @yield('content')
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <div id="footer-Style">
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-12" id="colImgStyle">
            <img src="https://fotos.subefotos.com/d80e7950b669a853e0351403ea9c873ao.png" alt="" srcset="">
          </div>
          <div class="col-12" id="footerCopy">
          © KRZ S.A: Creador de Aplicacion y Páginas WEB<br>
            Web alojado en Nominalia
          </div>
        </div>
      </div>
    </footer>
  </div>
</body>

</html>