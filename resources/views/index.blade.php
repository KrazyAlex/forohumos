
@extends('master')

@section('style')
<style>
    
</style>
@endsection

@section('script')

<script defer>
    window.onload = function() {
        document.getElementById("homeNav").className += ' active';
    };
    

</script>
@endsection


@section('content')
    <h4 style="float: left">Hilos</h4>
    <button onclick="window.location.href = '/mainPage/create/hilo';" class="btn btn-outline-success mb-2" id="btnCreate">Crear hilo</button>
    <table id="tableINDX" class="table table-striped" data-order='[[ 3, "desc" ]]'>
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Tema</th>
                <th scope="col">Likes</th>
                <th scope="col">Fecha Creacion</th>
                <th scope="col">Creador</th>
                <th scope="col" class="lastCol"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($hilos as $hilo)
                <tr onclick="window.location.href = '/mainPage/hilo/'+{{$hilo->id}};">
                    <th scope="row">{{ $hilo->id }}</th>
                    <td>{{ $hilo->tema }}</td>
                    <td>{{ $hilo->likes }}</td>
                    <td>{{ $hilo->created_at }}</td>
                    <td class="creatorCol">
                        <div class="row">
                            <div class="col-sm-2 col-12">
                                <img class="userIMG" src="{{$hilo->img}}" >
                            </div>
                            <div class="col-md-2">{{ $hilo->nomUsuario }}</div>
                        </div> 
                    </td>
                    <td class="lastCol"></td>
                    <!--
                        @if( $usuario->id  === 1 || $usuario->nomUsuario === $hilo->nomUsuario )
                            <button onclick="window.location.href = '/mainPage/delete/'+{{$hilo->id}};" type="submit" class="btn btn-outline-danger my-2 my-sm-0">Borrar</button>
                        @endif-->
                </tr>
            @endforeach
            
        </tbody>
        <tfoot>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Tema</th>
                <th scope="col">Likes</th>
                <th scope="col">Fecha Creacion</th>
                <th scope="col">Creador</th>
                <th scope="col" class="lastCol"></th>
            </tr>
        </tfoot>
    </table>
@endsection
        