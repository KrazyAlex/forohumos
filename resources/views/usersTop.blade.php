
@extends('master')

@section('style')
<style>
    .hbtn{
        background-color: #343a40;
        border-color: #343a40;
    }
</style>
@endsection

@section('script')

<script defer>

    window.onload = function() {
        document.getElementById("userTop").className += ' active';
    };
    

</script>
@endsection


@section('content')
    <h4 style="float: left">Top Usuarios con mas aportacion</h4>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Nombre Usuario</th>
                <th scope="col">Num de Hilos Creados</th>
                <th scope="col">Cuando se Unio </th>
                <th scope="col" class="lastCol"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($usuarios as $user)
                <tr onclick="window.location.href = '/userstop/show/'+'{{$user->nomUsuario}}';">
                    <th>{{$user->id}}</th>
                    <th><img class="userIMG" src="{{$user->img}}" >  {{$user->nomUsuario}}</th>
                    <th>{{$user->numH}}</th>
                    <th>{{substr($user->created_at, 0, 10)}}</th>
                    <th class="lastCol"> </th>
                </tr>

            @endforeach
        </tbody>
    </table>
@endsection
        