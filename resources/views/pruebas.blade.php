@extends('master')

@section('content')

<h4>Lista de Usuarios</h4>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">id</th>
        <th scope="col">Nombre Usuario</th>
        <th scope="col">Correo</th>
        <th scope="col">Nombre</th>
        <th scope="col">Apellido</th>
        <th></th>
      </tr>
    </thead>
    <tbody>

        @if ($usuarios->isEmpty())
        <tr>
            <td>No hay usuarios</td>
        </tr>
        @else
            @foreach($usuarios as $user)
            
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->nomUsuario }}</td>
                    <td>{{ $user->correo }}</td>
                    <td>{{ $user->nombre }}</td>
                    <td>{{ $user->apellido }}</td>
                    <td>
                        <button onclick="window.location.href = '/users/delete/'+{{$user->id}};" type="submit" class="btn btn-outline-danger my-2 my-sm-0">Borrar</button>
                    </td>
                </tr>

            @endforeach

        @endif

      
    </tbody>
  </table>

    
@endsection