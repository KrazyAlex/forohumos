@extends('master')
@section('script')


<script>
    function getUserToSend(){
        console.log('{{ $user->nomUsuario }}');
        return '{{ $user->nomUsuario }}';
    }

    function addFriend(){
        
        $.ajax({
            type: "POST",
            url: "/usertop/show/{{$user->nomUsuario}}/add",
            data: {
                id: '{{$user->id}}',
                _token: "{{csrf_token()}}"
            },
            success: () => window.location.reload(),
        });
    }
    function deleteFriend(){

        $.ajax({
            type: "POST",
            url: "/usertop/show/{{$user->nomUsuario}}/delete",
            data: {
                id: '{{$user->id}}',
                _token: "{{csrf_token()}}"
            },
            success: () => window.location.reload(),
        });
    }
</script>
<script defer src="../../js/app.js"></script>
@endsection

@section('style')
    <style>
        #presentation{
            margin-bottom:20px;
        }
        .showROW{
            margin: 0;
        }
        
        #pIMG{
            height: 200px;
            width: 200px;
            border: 1px solid #000000;
        }
        #friendBTN, #sendNewMSGBTN{
            background-color:#343a40;
            border-color:#343a40;
            margin-top: 5px;
            margin-left: 15px;
            
        }
        #sendNewMSGBTN{
            margin-top: -2px;
            margin-left: 25px;
        }
    </style>
@endsection


@section('content')
    <div class="content">
        <div class="row showROW"  id="presentation">
        
            @if($user->id == $usuario->id)
                <h5 class="col-12">{{$user->id}}__Tus propios Datos </h5>
            @else
                <h5 class="col-12">{{$user->id}}__Datos de {{$user->nomUsuario}}</h5>
            @endif
            
            <div class="col-lg-5 col-xl-3 col-md-4 col-12" id="personalIMG">
                <img id="pIMG" src="{{$user->img}}" > 
                <br>

                @if($user->id != $usuario->id)

                    @if(count($amigo) == 0) 
                        <button id="friendBTN" class="btn btn-primary mb-2" onClick="addFriend()">Añadir como amigo</button>
                    @else
                        <button id="friendBTN" class="btn btn-primary mb-2" onClick="deleteFriend()">Eliminar como amigo</button>
                        <div id="sendNewMSG"></div>
                    @endif

                @endif

                
            </div>

            <div class="col-12 col-md-7">
                <form>
                    <div class="form-group row">
                        <label for="staticNombre" class="col-sm-4 col-5 col-form-label">Descripcion</label>
                        <div class="col-sm-5 col-5">
                            <input disabled name="nombre" type="text" class="form-control-plaintext" id="staticNombre" value="{{ $user->descripcion }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticApellido" class="col-sm-4 col-5 col-form-label">Correo</label>
                        <div class="col-sm-5 col-5">
                            <input disabled name="apellido" type="text" class="form-control-plaintext" id="staticApellido" value="{{ $user->correo }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticFechaNac" class="col-sm-4 col-5 col-form-label">Se ha unido en la fecha: </label>
                        <div class="col-sm-5 col-5">
                            <input disabled name="fechaNac" type="date" class="form-control-plaintext" id="staticFechaNac" value="{{ $user->created_at }}">
                        </div>
                    </div>
                </form>
            </div>

        </div>
        
        <div class="row showROW">
            
            @if($user->id == $usuario->id)
                <h4 style="float: left">Hilos Creados por ti </h4>
            @else
                <h4 style="float: left">Hilos Creados por {{$user->nomUsuario}}</h4>
            @endif
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Tema</th>
                        <th scope="col">Num_Likes</th>
                        <th scope="col">Fecha Creacion</th>
                        <th scope="col">Creador</th>
                        <th scope="col" class="lastCol"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($hilos as $hilo)
                        <tr onclick="window.location.href = '/mainPage/hilo/'+{{$hilo->id}};">
                            <th scope="row">{{ $hilo->id }}</th>
                            <td>{{ $hilo->tema }}</td>
                            <td>{{ $hilo->likes }}</td>
                            <td>{{ $hilo->created_at }}</td>
                            <td>{{ $hilo->nomUsuario }}</td>
                            <td class="lastCol">
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
@endsection
