@extends('master')

@section('script')

<script defer src="../../js/app.js"></script>
@endsection

@section('style')
    <style>
        #createBTN{
            background-color: #343a40;
        }
        #createBTN:hover{
            background-color:#ffb62c;
        }
    </style>
@endsection
@section('content')

<form  method="POST">
    @csrf
    <h1 class="h3 mb-3 font-weight-normal">Creacion de hilo</h1>
    <label for="inputTema" class="sr-only">Tema</label>
    <input name="tema" required type="text" id="inputTema" class="form-control" placeholder="Tema">
    <br>
    <label for="inputTexto" class="sr-only">Texto</label>
    <input name="texto" required type="text" id="inputTexto" class="form-control" placeholder="Texto a comentar.....">
    <br>
    <label for="inputEtiqueta" class="sr-only">Etiqueta</label>
    <select name="etiqueta" id="inputEtiqueta">
        @foreach($etiquetas as $etiqueta)
            <option value="{{$etiqueta->id}}">{{$etiqueta->nombre}}</option>
        @endforeach
    </select>
    <br>
    <br>
    <div class="row" >
        <div class="col-4 offset-4">
            <button id="createBTN" class=" btn btn-lg btn-outline-warning btn-block" type="submit" >Enviar</button>
        </div>
    </div>
</form>
<br><br><br>
@endsection