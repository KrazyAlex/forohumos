

@extends('master')

@section('style')
    <style>
        #personalIMG{
            width: 200px;
        }
        #pIMG{
            height: 200px;
            width: 200px;
            border: 1px solid #000000;
        }
        #personalFORM{
            margin-left: 15px;
        }
        
        .form-control-plaintext{
            width:auto;
        }
        .editROW{
            margin: 0;
        }

        #fileUP{
            font-size: 13px;
            margin-bottom: 5px;
        }

        
    </style>
@endsection

@section('script')
        <script>
            function onIMGUP(data){

                var reader = new FileReader();

                reader.onloadend = function() {
                    console.log(this);
                    document.getElementById("comodin64").value=reader.result;
                    
                    console.log(this);
                };

                const say = document.getElementById("errorControl");

                if((data.files[0].size/1024)>40){

                    document.getElementById("fileUP").value = "";
                    say.innerText  = "El fichero pesa demasiado";
                    say.style.display = "block";

                }else {
                    say.innerText  = "";
                    say.style.display = "hidden";
                    reader.readAsDataURL(data.files[0]);

                }
            }

            function onPost(data){
                
                const say = document.getElementById("errorControl");

                if(data[3].value == ""){

                    say.innerText  = "No ha añadido ningun fichero";
                    say.style.display = "block";
                    return false;

                }else {

                    say.innerText  = "";
                    say.style.display = "hidden";
                    return true;
                }
            }
        </script>
@endsection


@section('content')
    <h3>Editando {{ $usuario->nomUsuario }}</h3>
    <div class="content">
        <div class="row editROW">
            <div class="col-md-3 col-12" id="personalIMG">
                <img id="pIMG" src="{{$usuario->img}}" > 
                <br>
                <div>
                    <h5 >Actualizar foto de perfil</h5>
                    <form action="/userPage/updateIMG" onsubmit="return onPost(this)" method="post">
                        @csrf
                        <h6>(Max 40Mb) Recomendado: 200 x 200</h6>
                        <input max-size="50000" id="fileUP" type="file" class="form-control-file" default="subir" onchange="onIMGUP(this)" require accept="image/*">
                        <h6 id="errorControl"></h6>
                        <input type="submit" class="btn btn-primary mb-2" value="Subir">
                        <input type="text" class="invisible" id="comodin64" name="fileUP">
                    </form>
                </div>
            </div>
            <div class="col-7" id="personalFORM">
                <h5 >Actualizar Datos Usuario</h5>
                <form  method="POST">
                    @csrf
                    <div class="form-group row">
                        <label for="staticNombre" class="col-sm-5 col-5 col-form-label">Nombre</label>
                        <div class="col-sm-5 col-5">
                        <input name="nombre" type="text" class="form-control-plaintext" id="staticNombre" value="{{ $usuario->nombre }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticApellido" class="col-sm-5 col-5 col-form-label">Apellido</label>
                        <div class="col-sm-5 col-5">
                            <input name="apellido" type="text" class="form-control-plaintext" id="staticApellido" value="{{ $usuario->apellido }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticFechaNac" class="col-sm-5 col-5 col-form-label">Fecha Nacimiento</label>
                        <div class="col-sm-5 col-5">
                            <input name="fechaNac" type="date" class="form-control-plaintext" id="staticFechaNac" value="{{ $usuario->fechaNac }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticContraseña" class="col-sm-5 col-5 col-form-label">Nueva Contraseña</label>
                        <div class="col-sm-5 col-5">
                            <input name="password" type="password" class="form-control-plaintext" id="staticContraseña" value="{{ $usuario->contraseña }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticDescripcion" class="col-sm-5 col-5 col-form-label">Descripcion</label>
                        <div class="col-sm-5 col-5">
                        <textarea name="descripcion" rows="2" cols="50" maxlength="255" class="form-control-plaintext" id="staticDescripcion">{{ $usuario->descripcion }}</textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Guardar</button>
                </form>
            </div>
        </div>
        <div class="row editROW">
            <h4 style="float: left">Tus Hilos</h4>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Tema</th>
                        <th scope="col">Num_Likes</th>
                        <th scope="col">Fecha Creacion</th>
                        <th scope="col">Creador</th>
                        <th scope="col" class="lastCol"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($hilos as $hilo)
                        <tr onclick="window.location.href = '/mainPage/hilo/'+{{$hilo->id}};">
                            <th scope="row">{{ $hilo->id }}</th>
                            <td>{{ $hilo->tema }}</td>
                            <td>{{ $hilo->likes }}</td>
                            <td>{{ $hilo->created_at }}</td>
                            <td>{{ $hilo->nomUsuario }}</td>
                            <td class="lastCol">
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    

@endsection