@extends('master')

@section('style')
<style>
    .active-pink-4 input[type=text]:focus:not([readonly]) {
        border: 1px solid #343a40;
        box-shadow: 0 0 0 1px #343a40;
    }
</style>
@endsection

@section('script')

<script defer>

    window.onload = function() {
        var activeone=document.getElementById("searchNav");
        activeone.className += ' active';
    };
</script>
@endsection

@section('content')
    <h4 style="float: left">Buscar Hilo</h4>

    <form method="POST" class="active-pink-4 mb-4">
        @csrf
        <input class="form-control" type="text" name="text" placeholder="Buscar" aria-label="Search">
    </form>

    <br>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Tema</th>
                <th scope="col">Num_Likes</th>
                <th scope="col">Fecha Creacion</th>
                <th scope="col">Creador</th>
                <th scope="col" class="lastCol"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($hilos as $hilo)
                <tr onclick="window.location.href = '/mainPage/hilo/'+{{$hilo->id}};">
                    <th scope="row">{{ $hilo->id }}</th>
                    <td>{{ $hilo->tema }}</td>
                    <td>{{ $hilo->likes }}</td>
                    <td>{{ $hilo->created_at }}</td>
                    <td class="creatorCol">
                        <div class="row">
                            <div class="col-sm-2 col-12">
                            <img class="userIMG" src="{{$hilo->img}}" >
                            </div>
                            <div class="col-md-2">{{ $hilo->nomUsuario }}</div>
                        </div> 
                    </td>
                    <td class="lastCol">
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection