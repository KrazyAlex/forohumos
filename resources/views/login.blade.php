<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <link rel="icon" type="image/ico" href="/logIco.ico" />
    <title>Login</title>
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }
        body{
            background-image:url('/Back.png');
            background-repeat: repeat;
        }
        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: white;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .white{
            color: white;
        }

        .blue{
            color: blue;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        #loginContent{
            background-color: #343a40;
            padding: 25px;
            border-radius: 12px 12px 12px 12px;
            -moz-border-radius: 12px 12px 12px 12px;
            -webkit-border-radius: 12px 12px 12px 12px;
            border: 1px solid #000000;
        }

        h1{
            color: #e9ecef;
        }

    </style>
</head>
<body>
    <div class="flex-center position-ref full-height">
        <form id="loginContent" class="formulario_login" action="/login" method="POST">
            @csrf
            <h1 class="h3 mb-3 font-weight-normal">Log In</h1>
            <label for="inputUsuario" class="sr-only">Nombre de Usuario</label>
            <input required name="username" type="text" id="inputUsuario" class="form-control" placeholder="Nombre de usuario">
            @isset($error)
                @if($error == 1)
                    No existe el usuario <br>
                @endif
            @endisset
            <br>
            <label for="inputContraseña" class="sr-only">Contraseña</label>
            <input required name="password" type="password" id="inputContraseña" class="form-control" placeholder="Contraseña">
            @isset($error)
                @if($error == 2)
                    Contraseña Erronea <br>
                @endif
            @endisset
            <br>
            <button class="btn btn-lg btn-outline-warning btn-block" type="submit">Log in</button>
            <a href="/" class="btn btn-s btn-outline-light btn-block" >Volver</a>
        </form>
        
    </div>
</body>
</html>