import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import firebase from './Firebase/fire';

import ChatComponent from './ReactComponents/chatComponent';

let globalActualUser;

class Chat extends Component {
    constructor(){
        super();
        this.state = {
            actualUser:  getUsername(),
            data: {
                from: [],
                to: []
            }
        }

        globalActualUser = this.state.actualUser;

    }
    sendMesagge(data){
        
        data.preventDefault();
        const objectToSend={
            to:  data.target[0].defaultValue,
            from: globalActualUser,
            texto:  data.target[1].value,
            time: new Date
        }
        firebase.firestore().collection("Chat").doc().set(objectToSend);
        data.target[1].value = "";

    }

    componentDidMount(){
        const data = this.state.data;
        firebase.firestore().collection("Chat").where("from", "==", this.state.actualUser).onSnapshot((snapshot) => {
            data.from =  snapshot.docs;
            this.setState( {data: data}); 
        })
        firebase.firestore().collection("Chat").where("to", "==", this.state.actualUser).onSnapshot((snapshot) => {
            data.to =  snapshot.docs;
            this.setState( {data: data});
        })
    }
    

    render(){
        console.log(this.state);
            
        return (
            <div >
                <ChatComponent
                    msg={this.state.data}
                    actualUser={this.state.actualUser}
                    sendMSG={this.sendMesagge}
                />
            </div>
        );
    }
}

export default Chat;

ReactDOM.render(<Chat/>, document.getElementById('chatBox'));

class NewMSG extends Component {
    constructor(){
        super();
        this.state = {
            sendTo:  getUserToSend(),
        }
        
    }

    sendNewMSG() {

        const data = document.getElementById('staticMSG');
        console.log(data);
        const error = document.getElementById('errorControl');
        if(data.value == ""){
            error.innerText  = "No mandes un mensaje vacio";
            error.style.display = "block";
            return;
        } else {
            error.innerText  = "";
            error.style.display = "hidden";
        }
        
        const objectToSend={
            to:  this.state.sendTo,
            from: globalActualUser,
            texto:  data.value,
            time: new Date
        }
        data.value = "";
        firebase.firestore().collection("Chat").doc().set(objectToSend);

        $('#newMSGModal').modal('toggle');
    }

    closeModal(){
        const error = document.getElementById('errorControl');
        error.innerText  = "";
        error.style.display = "hidden";
        
    }
    render(){
        return(
            <div>
                <button id="sendNewMSGBTN" className="btn btn-primary mb-2" data-toggle="modal" data-target="#newMSGModal">Mandar Mensaje</button>

                <div id="newMSGModal" className="modal" tabIndex="-1" role="dialog">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Mandar Mensaje a {this.state.sendTo}</h5>
                            <button onClick={()=> this.closeModal()} type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span  aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <textarea  name="MSG" rows="2" cols="50" maxLength="255" className="form-control-plaintext" id="staticMSG"></textarea>
                            <h6 id="errorControl"></h6>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" onClick={()=> this.sendNewMSG()}>Enviar</button>
                            <button type="button" className="btn btn-secondary" onClick={()=> this.closeModal()} data-dismiss="modal">Close</button>
                        </div>
                        </div>
                    </div>
                    </div>
            </div>
        )
    }

}
if(document.getElementById('sendNewMSG') != null){
    ReactDOM.render(<NewMSG/>, document.getElementById('sendNewMSG'));

}

class LikeContrl extends Component{
    constructor(){
        super();
        this.state = {
            liked:  getLiked(),
        }
        
    }

    giveLike(){
        console.log('like');
        $.ajax({
            url:getId()+'/like'
        }).done(function (result) {
            document.getElementById('numLikes').innerHTML = result;
        });
        this.setState({liked: 1});
    }

    disLike(){
        console.log('disslike');
        $.ajax({
            url:getId()+'/dislike'
        }).done(function (result) {
            document.getElementById('numLikes').innerHTML = result;
        });
        this.setState({liked: 0});
    }


    render(){
        if(this.state.liked == 1){
            return (
                <div>
                    <div onClick={()=> this.disLike()} id="likeBTN" class="likeOrange" src="/like_orange.ico" alt=""></div>
                </div>
            );
        } else {
            return (
                <div>
                    <div onClick={()=> this.giveLike()} id="likeBTN" class=" likeGray" src="/like_gray.ico" alt=""></div>
                </div>
            );
        }
    }
}

if(document.getElementById('likeShow') != null){
    ReactDOM.render(<LikeContrl/>, document.getElementById('likeShow'));

}
