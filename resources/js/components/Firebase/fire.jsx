import firebase from 'firebase/app';
import 'firebase/firestore';

 const DB_CONFIG = {
  apiKey: "AIzaSyCXE0hnmA8vbWm1QVdpqPSFiXgeOLXMg8c",
  authDomain: "forohumo-chat.firebaseapp.com",
  databaseURL: "https://forohumo-chat.firebaseio.com",
  projectId: "forohumo-chat",
  storageBucket: "forohumo-chat.appspot.com",
  messagingSenderId: "237617941383",
  appId: "1:237617941383:web:cd896b4cbc5d4e5543a122",
  measurementId: "G-JRTV5HRC67"
}

firebase.initializeApp(DB_CONFIG);

export default firebase;