import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import DateComparer from './dateComparer';
import dates from './dateComparer';


// FROM = enviados por mi
// TO = recibidos


let globalThis = null;

class ChatComponent extends Component {



    constructor() {
        super()
        this.state = {
            data: [],
            activeChat: [],
        }
        globalThis = this;
    }

    componentWillReceiveProps() {
        this.order(this.props.msg);
    }

    componentDidUpdate() {
        const allchats = globalThis.state.data;
        const activeChats = globalThis.state.activeChat;


        allchats.forEach(chat => {
            const cSelect = chat.friend;
            var msgContent = document.getElementById("chatResp" + cSelect);
            var msgForm = document.getElementById("chatRespForm" + cSelect);
            msgContent.className = "col-12 chatResponser invisible";
            msgForm.className = "col-12 chatResponserForm invisible";
        })

        activeChats.forEach(active => {
            console.log("active",active);
            var msgContent = document.getElementById("chatResp" + active);
            var msgForm = document.getElementById("chatRespForm" + active);
            console.log(msgContent.className);
            msgContent.className = msgContent.className.replace(" invisible", "");
            msgForm.className = msgForm.className.replace(" invisible", "");
            
            msgContent.scrollTop = msgContent.scrollHeight;
        });
    }


    order(data) {
        const { to, from } = data;
        var list = [];

        to.forEach(msg => {
            const todata = msg.data();

            if (list.length == 0) {
                list.push({
                    friend: todata.from,
                    msg: [todata]
                })
            } else {
                var encontrado = false;
                list.forEach((user, index) => {
                    if (!encontrado) {
                        if (user.friend == todata.from) {
                            if (user.msg.find(message => message.time == todata.time && message.text == todata.text) != undefined) {
                                console.log("ya existente")
                                encontrado = true;
                                return;
                            }
                            user.msg.push(todata);
                            encontrado = true;
                            return;
                        }
                        if (index == list.length - 1) {
                            list.push({
                                friend: todata.from,
                                msg: [todata]
                            })
                        }
                    }
                })

            }
        });

        from.forEach(msg => {
            const formdata = msg.data();

            if (list.length == 0) {
                list.push({
                    friend: formdata.to,
                    msg: [formdata]
                })
            } else {
                var encontrado = false;
                list.forEach((user, index) => {
                    if (!encontrado) {
                        if (user.friend == formdata.to) {
                            user.msg.push(formdata);
                            encontrado = true;
                            return;
                        }
                        if (index == list.length - 1) {
                            list.push({
                                friend: formdata.to,
                                msg: [formdata]
                            })
                        }
                    }
                })
            }
        })

        list.forEach(items => {
            items.msg.sort(function (a, b) {
                const dateA = new Date(a.time.seconds * 1000)
                const dateB = new Date(b.time.seconds * 1000)


                return DateComparer.compare(dateA, dateB);
            })
        })


        this.state.data = list;
        console.log("list", list);

    }

    openMsgReponder(op) {
        const activeChats = globalThis.state.activeChat;
        console.log("yo", op.target.parentElement.children);
        const parent = op.target.parentElement.children;
        if (activeChats.includes(parent[0].innerText)) {
            activeChats.splice(activeChats.findIndex(element => element == parent[0].innerText), 1)

        } else {
            activeChats.push(parent[0].innerText);
            parent[1].scrollTop = parent[1].scrollHeight;
        }
        globalThis.forceUpdate()
    }

    render() {
        const list = this.state.data;

        const html = list.map((item, key) => {
            const id = "chatResp" + item.friend;
            const id2= "chatRespForm" + item.friend
            return (
                <div className="row" key={"FN" + key}>
                    <div className="col-12 friendName" onClick={this.openMsgReponder}>
                        {item.friend}
                    </div>
                    <div className="col-12 chatResponser" id={id} >
                        <div className="container-fluid">
                            {
                                item.msg.map((item, key) => {

                                    if (item.from == this.props.actualUser) {

                                        return (
                                            <div className="row msgI">
                                                <div key={item.from+"-from" + key} className="col-12 msgResponse msgFROM ">
                                                    {item.texto}
                                                </div>
                                            </div>
                                        )
                                    } else {
                                        return (
                                            <div className="row msgD">
                                                <div key={item.to+"-to" + key} className="col-12 msgResponse msgTO">
                                                    {item.texto}
                                                </div>
                                            </div>
                                        )
                                    }

                                })
                            }
                        </div>
                    </div>

                    <div className="col-12 chatResponserForm" id={id2}>
                        <form onSubmit={this.props.sendMSG} >
                            <input type="text" name="to" defaultValue={item.friend} id="msgToINV" />
                            <input required type="text" name="msg" id="msgTEXT" />
                            <button type="submit">
                                <svg className="bi bi-box-arrow-in-up" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fillRule="evenodd" d="M4.646 7.854a.5.5 0 00.708 0L8 5.207l2.646 2.647a.5.5 0 00.708-.708l-3-3a.5.5 0 00-.708 0l-3 3a.5.5 0 000 .708z" clipRule="evenodd" />
                                    <path fillRule="evenodd" d="M8 15a.5.5 0 00.5-.5v-9a.5.5 0 00-1 0v9a.5.5 0 00.5.5z" clipRule="evenodd" />
                                    <path fillRule="evenodd" d="M1.5 2.5A1.5 1.5 0 013 1h10a1.5 1.5 0 011.5 1.5v8A1.5 1.5 0 0113 12h-1.5a.5.5 0 010-1H13a.5.5 0 00.5-.5v-8A.5.5 0 0013 2H3a.5.5 0 00-.5.5v8a.5.5 0 00.5.5h1.5a.5.5 0 010 1H3a1.5 1.5 0 01-1.5-1.5v-8z" clipRule="evenodd" />
                                </svg>
                            </button>
                        </form>
                    </div>
                </div>)
        })


        return (
            <div className="container">
                <div className="row">
                    <div className="col-12 friendName">
                            Mensajes de Amigos
                    </div>
                </div>
                {html}
            </div>

            );
    }
}

export default ChatComponent;
